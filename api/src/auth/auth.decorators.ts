import { createParamDecorator, CustomDecorator, ExecutionContext, SetMetadata, UseGuards } from '@nestjs/common';
import { AuthenticationGuard } from './guards/authentication.guard';
import { Roles } from '../users/users.constants';
import { NO_AUTHORIZATION_KEY, ROLE_KEY } from './auth.constants';

export const Authenticate = () => UseGuards(AuthenticationGuard);

export const AuthUser = createParamDecorator((_, ctx: ExecutionContext) => {
  const { user } = ctx.switchToHttp().getRequest();
  return user;
});

export const Role = (role: Roles): CustomDecorator<string> => SetMetadata(ROLE_KEY, role);

export const NoAuthorization = () => SetMetadata(NO_AUTHORIZATION_KEY, true);
