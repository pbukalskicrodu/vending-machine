import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Cfg } from '../config/config.decorators';
import { Config } from '../config/config';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    @Cfg() private readonly config: Config,
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) {}

  async authenticate(username: string, password: string): Promise<User> {
    return this.usersService.getByCredentials(username, password);
  }

  async authorize({ sub }): Promise<User> {
    return this.usersService.getById(sub);
  }

  createAccessToken({ id }: User): string {
    const { auth: { expiresIn } } = this.config;
    return this.jwtService.sign(
      {},
      {
        expiresIn,
        subject: id.toString(),
      },
    );
  }
}
