import { Strategy as LocalStrategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { Strategies } from  '../auth.constants';
import { AuthService } from '../auth.service';
import { User } from '../../users/user.entity';

@Injectable()
export class AuthenticationStrategy extends PassportStrategy(LocalStrategy, Strategies.Local) {
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(username: string, password: string) {
    if (!username || !password) {
      throw new NotFoundException();
    }
    const user: User = await this.authService.authenticate(username, password);
    if (!user) {
       throw new UnauthorizedException();
    }
    return user;
  }
}
