import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy as JwtStrategy, StrategyOptions } from 'passport-jwt';
import { Strategies } from '../auth.constants';
import { AuthService } from '../auth.service';
import { User } from '../../users/user.entity';

@Injectable()
export class AuthorizationStrategy extends PassportStrategy(JwtStrategy, Strategies.Jwt) {
  constructor(
    private readonly options: Partial<StrategyOptions>,
    private readonly authService: AuthService,
  ) {
    super({
      ...options,
      ignoreExpiration: false,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }

  async validate(token) {
    const user: User = await this.authService.authorize(token);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
