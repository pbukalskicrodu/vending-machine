import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { JsonWebTokenError } from 'jsonwebtoken';
import { NO_AUTHORIZATION_KEY, ROLE_KEY, Strategies } from '../auth.constants';
import { Roles } from '../../users/users.constants';

@Injectable()
export class AuthorizationGuard extends AuthGuard(Strategies.Jwt) {
  constructor(private readonly reflector: Reflector) {
    super();
  }

  getReflectorValue(key: string, context: any): any {
    return this.reflector.getAllAndOverride<any>(key, [
      context.getHandler(),
      context.getClass(),
    ]);
  }

  handleRequest(error: Error, user, info: JsonWebTokenError, context: ExecutionContext) {
    if (context.getType() === 'http') {
      if (user) {
        const role: Roles = this.getReflectorValue(ROLE_KEY, context);
        if (role && role !== user.role) {
          throw new UnauthorizedException();
        }
        return user;
      }
      const noAuthorization = this.getReflectorValue(NO_AUTHORIZATION_KEY, context);
      if (noAuthorization) {
        return;
      }
      throw new UnauthorizedException();
    }
  }
}
