import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Strategies } from '../auth.constants';

@Injectable()
export class AuthenticationGuard extends AuthGuard(Strategies.Local) {}
