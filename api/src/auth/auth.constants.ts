export const NO_AUTHORIZATION_KEY = 'noAuthorization';
export const ROLE_KEY = 'role';
export const SALT_ROUNDS = 10;

export enum Strategies {
  Local = 'local',
  Jwt = 'jwt',
}
