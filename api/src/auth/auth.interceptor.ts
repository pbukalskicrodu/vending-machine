import { CallHandler, Injectable, NestInterceptor } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../users/user.entity';

@Injectable()
export class AuthInterceptor implements NestInterceptor {
  constructor(private readonly authService: AuthService) {}

  intercept(_, next: CallHandler): Observable<any> {
    return next.handle().pipe(map((user: User): object => {
      try {
        return {
          accessToken: this.authService.createAccessToken(user),
          user,
        };
      } catch (err) {
        return user;
      }
    }));
  }
}
