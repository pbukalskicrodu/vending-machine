import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { Config } from '../config/config';
import { CONFIG } from '../config/config.constants';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { AuthenticationStrategy } from './strategies/authentication.strategy';
import { AuthorizationStrategy } from './strategies/authorization.strategy';
import { APP_GUARD } from '@nestjs/core';
import { AuthorizationGuard } from './guards/authorization.guard';

@Module({
  imports: [
    JwtModule.registerAsync({
      useFactory: async ({ auth: { secret } }: Config) => ({ secret }),
      inject: [CONFIG],
    }),
    UsersModule,
  ],
  controllers: [],
  providers: [
    AuthService,
    AuthenticationStrategy,
    {
      provide: AuthorizationStrategy,
      useFactory: async (
        { auth: { secret } }: Config,
        authService: AuthService,
      ): Promise<AuthorizationStrategy> =>
        new AuthorizationStrategy(
          { secretOrKey: secret },
          authService,
        ),
      inject: [CONFIG, AuthService],
    },
    {
      provide: APP_GUARD,
      useClass: AuthorizationGuard,
    },
  ],
  exports: [AuthService],
})
export class AuthModule {}
