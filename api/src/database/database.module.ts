import { TypeOrmModule } from '@nestjs/typeorm';
import { Config } from '../config/config';
import { CONFIG } from '../config/config.constants';
import { Product } from '../products/product.entity';
import { User } from '../users/user.entity';

export const DatabaseModule = TypeOrmModule.forRootAsync({
  useFactory: ({ mysql: { url } }: Config) => ({
    type: 'mysql',
    url,
    entities: [Product, User],
  }),
  inject: [CONFIG],
});
