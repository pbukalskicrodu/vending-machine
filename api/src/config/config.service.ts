import { Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';
import { Config } from './config';
import { Environment } from './environment';

@Injectable()
export class ConfigService {
  static getFromEnv() {
    const env = plainToClass(Environment, process.env);
    const config = plainToClass(
      Config,
      {
        apiVersion: env.API_VERSION,
        auth: {
          expiresIn: env.AUTH_EXPIRES_IN,
          ...this.extractFromSecret(env.AUTH_SECRET),
          secret: env.AUTH_SECRET,
        },
        mysql: {
          url: env.MYSQL_URL,
        },
        port: env.PORT,
      },
      {
        enableImplicitConversion: true,
        exposeDefaultValues: true,
      },
    );

    const errors = validateSync(config, { skipMissingProperties: false });
    if (errors.length > 0) {
      throw new Error(errors.toString());
    }

    return config;
  }

  private static extractFromSecret(secret: string) {
    return Object.fromEntries(Object.entries({
      iv: 16,
      key: 32,
    }).map(([key, value]) => [key, ((length) => {
      const buffered = Buffer.from(secret).toString('hex');
      const multiplier = length / buffered.length;
      let finalSecret = '';

      for (let i = 0; i < multiplier; ++i) {
        finalSecret += buffered;
      }

      return finalSecret.slice(0, length);
    })(value)]));
  }
}
