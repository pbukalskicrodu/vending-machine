export class Environment {
  API_VERSION: number;
  AUTH_EXPIRES_IN: string;
  AUTH_SECRET: string;
  MYSQL_URL: string;
  PORT?: string;
}
