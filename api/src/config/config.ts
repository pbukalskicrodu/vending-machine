import { IsInt, IsPort, IsPositive, ValidateNested } from 'class-validator';
import { AuthConfig } from './auth.config';
import { MysqlConfig } from './mysql.config';

export class Config {
  @IsInt()
  @IsPositive()
  apiVersion: number;

  @ValidateNested()
  auth: AuthConfig;

  @ValidateNested()
  mysql: MysqlConfig;

  @IsPort()
  port = '3000';
}
