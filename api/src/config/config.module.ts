import { Global, Module } from '@nestjs/common';
import { ConfigService } from './config.service';
import { CONFIG } from './config.constants';

@Global()
@Module({
  imports: [],
  controllers: [],
  providers: [
    ConfigService,
    {
      provide: CONFIG,
      useValue: ConfigService.getFromEnv(),
    },
  ],
  exports: [CONFIG],
})
export class ConfigModule {}
