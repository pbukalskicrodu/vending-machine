import { IsNotEmpty, IsString } from 'class-validator';

export class MysqlConfig {
  @IsNotEmpty()
  @IsString()
  url: string;
}
