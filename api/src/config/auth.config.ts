import { IsNotEmpty, IsString } from 'class-validator';

export class AuthConfig {
  @IsNotEmpty()
  @IsString()
  expiresIn: string;

  @IsNotEmpty()
  @IsString()
  iv: string;

  @IsNotEmpty()
  @IsString()
  key: string;

  @IsNotEmpty()
  @IsString()
  secret: string;
}
