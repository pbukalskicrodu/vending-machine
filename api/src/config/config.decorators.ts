import { Inject } from '@nestjs/common';
import { CONFIG } from './config.constants';

export const Cfg = () => Inject(CONFIG);
