import { IsInt, IsPositive } from 'class-validator';

export class BuyProductDto {
  @IsInt()
  @IsPositive()
  productId: number;

  @IsInt()
  @IsPositive()
  amount: number;
}
