import { IsInt, IsPositive, IsString, Min } from 'class-validator';

export class CreateProductDto {
  @IsInt()
  @Min(0)
  amountAvailable: number;

  @IsInt()
  @IsPositive()
  cost: number;

  @IsString()
  productName: string;
}
