import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ProductsService } from './products.service';
import { Coins } from '../app.constants';
import { UsersService } from '../users/users.service';
import { Product } from './product.entity';

describe('Products service', () => {
  let productsService: ProductsService;

  const expectChangeInCoins = (change: number, coins: number[]) => {
    expect(productsService.calculateChangeInCoins(change, Coins)).toEqual(coins);
  }

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        ProductsService,
        {
          provide: getRepositoryToken(Product),
          useValue: {},
        },
        {
          provide: UsersService,
          useValue: {},
        },
      ],
    }).compile();

    productsService = moduleRef.get<ProductsService>(ProductsService);
  });

  describe('Calculate change in coins', () => {
    test('0 => []', () => {
      expectChangeInCoins(0, []);
    });

    test('2 => []', () => {
      expectChangeInCoins(2, []);
    });

    test('32 => [20, 10]', () => {
      expectChangeInCoins(32, [20, 10]);
    });

    test('77 => [50, 20, 5]', () => {
      expectChangeInCoins(77, [50, 20, 5]);
    });

    test('399 => [100, 100, 100, 50, 20, 20, 5]', () => {
      expectChangeInCoins(399, [100, 100, 100, 50, 20, 20, 5]);
    });
  });
});
