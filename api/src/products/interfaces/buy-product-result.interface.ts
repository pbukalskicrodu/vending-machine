import { Product } from '../product.entity';

interface BuyProductResultProduct {
  id: number;
  amount: number;
}

export interface BuyProductResult {
  total: number;
  products: BuyProductResultProduct[];
  change: number[];
}
