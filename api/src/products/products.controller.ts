import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { ProductsService } from './products.service';
import { Product } from './product.entity';
import { AuthUser } from '../auth/auth.decorators';
import { User } from '../users/user.entity';
import { Role } from '../auth/auth.decorators';
import { Roles } from '../users/users.constants';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  async getProducts(@AuthUser() user: User): Promise<Product[]> {
    if (user.role === Roles.Seller) {
      return this.productsService.getByUser(user);
    }
    return this.productsService.getAll();
  }

  @Post()
  @Role(Roles.Seller)
  async createProduct(
    @AuthUser() user: User,
    @Body() data: CreateProductDto,
  ): Promise<Product> {
    return this.productsService.create(user, data);
  }

  @Put(':productId')
  @Role(Roles.Seller)
  async updateProduct(
    @AuthUser() user: User,
    @Param('productId', ParseIntPipe) productId: number,
    @Body() data: UpdateProductDto,
  ): Promise<Product> {
    return this.productsService.update(user, productId, data);
  }

  @Delete(':productId')
  @Role(Roles.Seller)
  async deleteProduct(
    @AuthUser() user: User,
    @Param('productId', ParseIntPipe) productId: number,
  ): Promise<number> {
    return this.productsService.delete(user, productId);
  }
}
