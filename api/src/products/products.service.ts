import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';
import { User } from '../users/user.entity';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { BuyProductDto } from './dto/buy-product.dto';
import { BuyProductResult } from './interfaces/buy-product-result.interface';
import { UsersService } from '../users/users.service';
import { Coins } from '../app.constants';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    private readonly usersService: UsersService,
  ) {}

  private async countById(id: number): Promise<number> {
    return this.productsRepository.count({ id });
  }

  async getAll(): Promise<Product[]> {
    return this.productsRepository.find();
  }

  async getByUser(user: User): Promise<Product[]> {
    return this.productsRepository.find({sellerId: user.id });
  }

  async getById(id: number): Promise<Product> {
    const product = await this.productsRepository.findOne({ id });
    if (!product) {
      throw new NotFoundException();
    }
    return product;
  }

  async create(user: User, data: CreateProductDto): Promise<Product> {
    const product = this.productsRepository.create({
      ...data,
      sellerId: user.id,
    });
    await this.productsRepository.save(product);
    return product;
  }

  async update(user: User, id: number, data: UpdateProductDto): Promise<Product> {
    const product = await this.getById(id);
    if (product.sellerId !== user.id) {
      throw new UnauthorizedException();
    }
    const updatedProduct = {
      ...product,
      ...data,
    };
    await this.productsRepository.save(updatedProduct);
    return updatedProduct;
  }

  async delete(user: User, id: number): Promise<number> {
    const product = await this.getById(id);
    if (product.sellerId !== user.id) {
      throw new UnauthorizedException();
    }
    await this.productsRepository.delete({ id });
    return id;
  }

  async buy(user: User, { amount, productId }: BuyProductDto): Promise<BuyProductResult> {
    const product = await this.getById(productId);
    const total = product.cost * amount;
    const change = user.deposit - total;
    if (change < 0) {
      throw new BadRequestException();
    }
    if (amount > product.amountAvailable) {
      throw new BadRequestException();
    }
    product.amountAvailable -= amount;
    await this.productsRepository.save(product);
    await this.usersService.reset(user.id);
    const changeInCoins = this.calculateChangeInCoins(change, Coins);
    const result: BuyProductResult = {
      total,
      products: [{
        id: productId,
        amount,
      }],
      change: changeInCoins,
    };
    return result;
  }

  calculateChangeInCoins(change: number, coins: number[]): number[] {
    const { coins: changeInCoins } = [...coins].reverse().reduce(({ coins, changeLeft }, coin) => {
      const coinsAmount = Math.floor(changeLeft / coin);
      const coinsValue = coin * coinsAmount;
      return {
        coins: [
          ...coins,
          ...Array(coinsAmount).fill(coin),
        ],
        changeLeft: changeLeft -= coinsValue,
      };
    }, { coins: [], changeLeft: change });
    return changeInCoins;
  }
}
