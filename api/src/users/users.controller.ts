import { Body, Controller, Delete, Get, Post, Put } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { NoAuthorization } from '../auth/auth.decorators';
import { AuthUser } from '../auth/auth.decorators';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async getUsers(): Promise<User[]> {
    return this.usersService.getAll();
  }

  @Get('me')
  async getUser(@AuthUser() user: User): Promise<User> {
    return user;
  }

  @Post()
  @NoAuthorization()
  async createUser(@Body() data: CreateUserDto): Promise<User> {
    return this.usersService.create(data);
  }

  @Put('me')
  async updateUser(@AuthUser() user: User, @Body() data: UpdateUserDto): Promise<User> {
    return this.usersService.update(user.id, data);
  }

  @Delete('me')
  async deleteUser(@AuthUser() user: User): Promise<void> {
    return this.usersService.delete(user.id);
  }
}
