import { IsEnum, IsString } from 'class-validator';
import { Roles } from '../users.constants';

export class CreateUserDto {
  @IsString()
  username: string;

  @IsString()
  password: string;

  @IsEnum(Roles)
  role: Roles;
}
