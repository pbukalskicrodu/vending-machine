import { IsIn } from 'class-validator';
import { Coins } from '../../app.constants';

export class DepositDto {
  @IsIn(Coins)
  deposit: number;
}
