import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { SALT_ROUNDS } from '../auth/auth.constants';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private usersRepository: Repository<User>) {}

  private async countById(id: number): Promise<number> {
    return this.usersRepository.count({ id });
  }

  async getAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async getById(id: number): Promise<User> {
    const user = await this.usersRepository.findOne({ id });
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async getByCredentials(username: string, password: string): Promise<User> {
    const user = await this.usersRepository.findOne({ username });
    if (!user) {
      throw new NotFoundException();
    }
    const passwordMatches = await bcrypt.compare(password, user.password);
    if (!passwordMatches) {
      throw new NotFoundException();
    }
    return user;
  }

  async create(data: CreateUserDto): Promise<User> {
    const user = this.usersRepository.create({
      ...data,
      deposit: 0,
      password: await this.hashPassword(data.password),
    });
    await this.usersRepository.save(user);
    return user;
  }

  async update(id: number, data: UpdateUserDto): Promise<User> {
    const user = await this.getById(id);
    const updatedUser = new User({
      ...user,
      ...data,
      ...(data?.password?.length && { password: await this.hashPassword(data.password) }),
    });
    await this.usersRepository.save(updatedUser);
    return updatedUser;
  }

  async delete(id: number): Promise<void> {
    const productsCount = await this.countById(id);
    if (!productsCount) {
      throw new NotFoundException();
    }
    await this.usersRepository.delete({ id });
  }

  async deposit(id: number, deposit: number): Promise<Pick<User, 'deposit'>> {
    const user = await this.getById(id);
    user.deposit += deposit;
    await this.usersRepository.save(user);
    return { deposit: user.deposit };
  }

  async reset(id: number): Promise<void> {
    const user = await this.getById(id);
    user.deposit = 0;
    await this.usersRepository.save(user);
  }

  async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, SALT_ROUNDS);
  }
}
