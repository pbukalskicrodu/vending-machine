import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';
import { Roles } from './users.constants';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  @Exclude()
  password: string;

  @Column()
  deposit: number;

  @Column()
  role: Roles;

  constructor(partial: Partial<User>) {
    Object.assign(this, partial);
  }
}
