import { Body, Controller, HttpCode, HttpStatus, Post, UseInterceptors } from '@nestjs/common';
import { ProductsService } from './products/products.service';
import { UsersService } from './users/users.service';
import { Authenticate, NoAuthorization } from './auth/auth.decorators';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AuthUser } from './auth/auth.decorators';
import { User } from './users/user.entity';
import { DepositDto } from './users/dto/deposit.dto';
import { BuyProductDto } from './products/dto/buy-product.dto';
import { BuyProductResult } from './products/interfaces/buy-product-result.interface';

@Controller()
export class AppController {
  constructor(
    private readonly productsService: ProductsService,
    private readonly usersService: UsersService,
  ) {}

  @Post('login')
  @Authenticate()
  @NoAuthorization()
  @UseInterceptors(AuthInterceptor)
  @HttpCode(HttpStatus.OK)
  async login(@AuthUser() user: User) {
    return user;
  }

  @Post('deposit')
  @HttpCode(HttpStatus.OK)
  async deposit(
    @AuthUser() user: User,
    @Body() { deposit }: DepositDto,
  ) {
    return this.usersService.deposit(user.id, deposit);
  }

  @Post('buy')
  @HttpCode(HttpStatus.OK)
  async buy(
    @AuthUser() user: User,
    @Body() data: BuyProductDto,
  ): Promise<BuyProductResult> {
    return this.productsService.buy(user, data);
  }

  @Post('reset')
  @HttpCode(HttpStatus.OK)
  async reset(@AuthUser() user: User): Promise<void> {
    await this.usersService.reset(user.id);
  }
}
