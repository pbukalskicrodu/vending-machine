import { ValidationPipe } from '@nestjs/common';

export class AppValidationPipe extends ValidationPipe {
  constructor() {
    super({
      disableErrorMessages: false,
      transform: true,
      whitelist: true,
    });
  }
}
