import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import * as rateLimiter from 'express-rate-limit';
import { AppValidationPipe } from './app.validation-pipe';
import { CONFIG } from './config/config.constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  // app.use(rateLimiter());
  // app.use(helmet());
  app.useGlobalPipes(new AppValidationPipe());
  const { apiVersion, port } = app.get(CONFIG);
  app.setGlobalPrefix(`/v${apiVersion}`);
  await app.listen(port);
}
bootstrap();
