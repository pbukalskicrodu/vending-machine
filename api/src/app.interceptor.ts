import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Request, Response } from 'express';
import { AuthorizationGuard } from './auth/guards/authorization.guard';

@Injectable()
export class AppInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const response: Response = context.switchToHttp().getResponse();
    response.set('Access-Control-Allow-Origin', '*');
    return next.handle().pipe(map(data => data?.data ? data : { data }));
  }
}
