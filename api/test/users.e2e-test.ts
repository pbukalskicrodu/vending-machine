import { TestingModule } from '@nestjs/testing';
import { INestApplication, NotFoundException } from '@nestjs/common';
import * as request from 'supertest';
import { createMockUser, headers } from './helpers';
import { User } from '../src/users/user.entity';
import { UsersModule } from '../src/users/users.module';
import { TestModule } from './test.module';
import * as faker from 'faker';
import { mockUser } from './mocks/user';
import { AuthModule } from '../src/auth/auth.module';
import { ConfigModule } from '../src/config/config.module';
import { UsersController } from '../src/users/users.controller';
import { AppController } from '../src/app.controller';
import { ProductsModule } from '../src/products/products.module';
import { Product } from '../src/products/product.entity';
import { UsersService } from '../src/users/users.service';

describe('Users module', () => {
  let app: INestApplication;
  let user: User;
  let usersService: UsersService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await TestModule({
      user,
      entities: [Product, User],
      metadata: {
        imports: [AuthModule, ConfigModule, ProductsModule, UsersModule],
        controllers: [AppController, UsersController],
      },
    }).compile();

    usersService = moduleRef.get<UsersService>(UsersService);

    app = moduleRef.createNestApplication();
    await app.init();
    user = await createMockUser(app, mockUser());
  });

  afterEach(async() => {
    await app.close();
  });

  describe('Get users', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await request(app.getHttpServer())
        .get('/users')
        .expect(401);
    });

    it('returns an array of users', async () => {
      const { body: { data } } = await request(app.getHttpServer())
        .get('/users')
        .set(headers(user))
        .expect(200);
      expect(data).toEqual([{ ...user, accessToken: undefined, password: undefined }]);
    });
  });

  describe('Create user', () => {
    it('throws 400 when username is not provided', async () => {
      await request(app.getHttpServer())
        .post('/users')
        .send({ ...mockUser(), username: undefined })
        .expect(400);
    });

    it('throws 400 when password is not provided', async () => {
      await request(app.getHttpServer())
        .post('/users')
        .send({ ...mockUser(), password: undefined })
        .expect(400);
    });

    it('throws 400 when role is not provided', async () => {
      await request(app.getHttpServer())
        .post('/users')
        .send({ ...mockUser(), role: undefined })
        .expect(400);
    });

    it('throws 400 when role is not valid', async () => {
      await request(app.getHttpServer())
        .post('/users')
        .send({ ...mockUser(), role: faker.random.word() })
        .expect(400);
    });

    it('creates a user', async () => {
      const mock = mockUser();
      const { body: { data } } = await request(app.getHttpServer())
        .post('/users')
        .send(mock)
        .expect(201);
      expect(data).toEqual({
        ...mock,
        id: 2,
        password: undefined,
        deposit: 0,
      });
    });
  });

  describe('Update user', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await request(app.getHttpServer())
        .put('/users/me')
        .send({ username: faker.internet.userName() })
        .expect(401);
    });

    it('updates a user', async () => {
      const update = mockUser();
      delete update.role;
      const { body: { data } } = await request(app.getHttpServer())
        .put('/users/me')
        .set(headers(user))
        .send(update)
        .expect(200);
      expect(data).toEqual({
        ...user,
        ...update,
        accessToken: undefined,
        password: undefined,
      });
    });
  });

  describe('Delete user', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await request(app.getHttpServer())
        .delete('/users/me')
        .expect(401);
    });

    it('deletes a user', async () => {
      await request(app.getHttpServer())
        .delete('/users/me')
        .set(headers(user))
        .expect(200);
      expect(usersService.getById(user.id)).rejects.toThrowError(NotFoundException);
    });
  });
});
