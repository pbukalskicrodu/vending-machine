import { TestingModule } from '@nestjs/testing';
import { INestApplication, NotFoundException } from '@nestjs/common';
import * as request from 'supertest';
import { createMockUser, headers } from './helpers';
import { User } from '../src/users/user.entity';
import { TestModule } from './test.module';
import * as faker from 'faker';
import { mockUser } from './mocks/user';
import { Product } from '../src/products/product.entity';
import { ProductsModule } from '../src/products/products.module';
import { ProductsService } from '../src/products/products.service';
import { mockProduct } from './mocks/product';
import { AppController } from '../src/app.controller';
import { AuthModule } from '../src/auth/auth.module';
import { ConfigModule } from '../src/config/config.module';
import { ProductsController } from '../src/products/products.controller';
import { UsersModule } from '../src/users/users.module';
import { UsersService } from '../src/users/users.service';
import { Roles } from '../src/users/users.constants';

describe('Products module', () => {
  let app: INestApplication;
  let user: User;
  let productsService: ProductsService;
  let usersService: UsersService;

  const createSeller = () => createMockUser(app, { ...mockUser(), role: Roles.Seller });

  beforeEach(async () => {
    const moduleRef: TestingModule = await TestModule({
      user,
      entities: [Product, User],
      metadata: {
        imports: [AuthModule, ConfigModule, ProductsModule, UsersModule],
        controllers: [AppController, ProductsController],
      },
    }).compile();

    productsService = moduleRef.get<ProductsService>(ProductsService);
    usersService = moduleRef.get<UsersService>(UsersService);

    app = moduleRef.createNestApplication();
    await app.init();
    user = await createMockUser(app, mockUser());
  });

  afterEach(async() => {
    await app.close();
  });

  describe('Get products', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await request(app.getHttpServer())
        .get('/products')
        .expect(401);
    });

    it('returns an array of products', async () => {
      const product = await productsService.create(user, mockProduct());
      const { body: { data } } = await request(app.getHttpServer())
        .get('/products')
        .set(headers(user))
        .expect(200);
      expect(data).toEqual([product]);
    });
  });

  describe('Create product', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await request(app.getHttpServer())
        .post('/products')
        .send(mockProduct())
        .expect(401);
    });

    it('throws 401 when user role is buyer', async () => {
      await request(app.getHttpServer())
        .post('/products')
        .set(headers(user))
        .send(mockProduct())
        .expect(401);
    });

    it('throws 400 when amount available is not provided', async () => {
      const seller = await createSeller();
      await request(app.getHttpServer())
        .post('/products')
        .set(headers(seller))
        .send({ ...mockProduct(), amountAvailable: undefined })
        .expect(400);
    });

    it('throws 400 when cost is not provided', async () => {
      const seller = await createSeller();
      await request(app.getHttpServer())
        .post('/products')
        .set(headers(seller))
        .send({ ...mockProduct(), cost: undefined })
        .expect(400);
    });

    it('throws 400 when product name is not provided', async () => {
      const seller = await createSeller();
      await request(app.getHttpServer())
        .post('/products')
        .set(headers(seller))
        .send({ ...mockProduct(), productName: undefined })
        .expect(400);
    });

    it('throws 400 when amount available is not valid', async () => {
      const seller = await createSeller();
      await request(app.getHttpServer())
        .post('/products')
        .set(headers(seller))
        .send({ ...mockProduct(), amountAvailable: -1 })
        .expect(400);
    });

    it('throws 400 when cost is not valid', async () => {
      const seller = await createSeller();
      await request(app.getHttpServer())
        .post('/products')
        .set(headers(seller))
        .send({ ...mockProduct(), cost: -1 })
        .expect(400);
    });

    it('creates a product', async () => {
      const seller = await createSeller();
      const mock = mockProduct();
      const { body: { data } } = await request(app.getHttpServer())
        .post('/products')
        .set(headers(seller))
        .send(mock)
        .expect(201);
      expect(data).toEqual({
        ...mock,
        id: 1,
        sellerId: seller.id,
      });
    });
  });

  describe('Update product', () => {
    it('throws 401 when authorization token is not provided', async () => {
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .put(`/products/${product.id}`)
        .send(mockProduct())
        .expect(401);
    });

    it('throws 401 when product is owned by a different seller', async () => {
      const seller = await createSeller();
      const product = await productsService.create(seller, mockProduct());
      const seller2 = await createSeller();
      await request(app.getHttpServer())
        .put(`/products/${product.id}`)
        .set(headers(seller2))
        .expect(401);
    });

    it('throws 400 when amount available is not valid', async () => {
      const seller = await createSeller();
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .put(`/products/${product.id}`)
        .set(headers(seller))
        .send({ amountAvailable: -1 })
        .expect(400);
    });

    it('throws 400 when cost is not valid', async () => {
      const seller = await createSeller();
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .put(`/products/${product.id}`)
        .set(headers(seller))
        .send({ cost: -1 })
        .expect(400);
    });

    it('throws 404 when product does not exist', async () => {
      const seller = await createSeller();
      const update = mockProduct();
      await request(app.getHttpServer())
        .put('/products/99')
        .set(headers(seller))
        .send(update)
        .expect(404);
    });

    it('updates a product', async () => {
      const seller = await createSeller();
      const product = await productsService.create(seller, mockProduct());
      const update = mockProduct();
      const { body: { data } } = await request(app.getHttpServer())
        .put(`/products/${product.id}`)
        .set(headers(seller))
        .send(update)
        .expect(200);
      expect(data).toEqual({
        ...product,
        ...update,
      });
    });
  });

  describe('Delete product', () => {
    it('throws 401 when authorization token is not provided', async () => {
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .delete(`/products/${product.id}`)
        .expect(401);
    });

    it('throws 401 when product is owned by a different seller', async () => {
      const seller = await createSeller();
      const product = await productsService.create(seller, mockProduct());
      const seller2 = await createSeller();
      await request(app.getHttpServer())
        .delete(`/products/${product.id}`)
        .set(headers(seller2))
        .expect(401);
    });

    it('throws 404 when product does not exist', async () => {
      const seller = await createSeller();
      await request(app.getHttpServer())
        .delete('/products/99')
        .set(headers(seller))
        .expect(404);
    });

    it('deletes a product', async () => {
      const seller = await createSeller();
      const product = await productsService.create(seller, mockProduct());
      await request(app.getHttpServer())
        .delete(`/products/${product.id}`)
        .set(headers(seller))
        .expect(200);
      expect(productsService.getById(product.id)).rejects.toThrowError(NotFoundException);
    });
  });
});
