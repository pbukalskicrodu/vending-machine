import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { CreateUserDto } from '../src/users/dto/create-user.dto';

export const headers = (user) => ({
  Authorization: `Bearer ${user.accessToken}`,
});

export const createMockUser = async (app: INestApplication, data: CreateUserDto) => {
  await request(app.getHttpServer())
    .post('/users')
    .send(data)
    .expect(201);
  const { body: { data: { accessToken, user } } } = await request(app.getHttpServer())
    .post('/login')
    .send(data)
    .expect(200);
  return { ...user, accessToken };
};
