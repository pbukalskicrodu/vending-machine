import * as faker from 'faker';
import { Roles } from '../../src/users/users.constants';

export const mockUser = () => ({
  username: faker.internet.userName(),
  password: faker.internet.password(),
  role: Roles.Buyer,
});
