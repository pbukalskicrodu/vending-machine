import * as faker from 'faker';

export const mockProduct = () => ({
  amountAvailable: faker.datatype.number(),
  cost: faker.datatype.number(),
  productName: faker.commerce.productName(),
});
