import { TypeOrmModule } from '@nestjs/typeorm';

export const TestDatabaseModule = (entities: Function | Function[]) => TypeOrmModule.forRoot({
  type: 'sqlite',
  database: ':memory:',
  entities: [].concat(entities),
  dropSchema: true,
  synchronize: true,
  // keepConnectionAlive: true,
});
