import { TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { createMockUser, headers } from './helpers';
import { User } from '../src/users/user.entity';
import { UsersModule } from '../src/users/users.module';
import { TestModule } from './test.module';
import { mockUser } from './mocks/user';
import { ProductsModule } from '../src/products/products.module';
import { Product } from '../src/products/product.entity';
import { Coins } from '../src/app.constants';
import { ProductsService } from '../src/products/products.service';
import { UsersService } from '../src/users/users.service';
import { mockProduct } from './mocks/product';
import * as faker from 'faker';
import { AppController } from '../src/app.controller';
import { AuthModule } from '../src/auth/auth.module';
import { ConfigModule } from '../src/config/config.module';

describe('App module', () => {
  let app: INestApplication;
  let user: User;
  let productsService: ProductsService;
  let usersService: UsersService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await TestModule({
      user,
      entities: [Product, User],
      metadata: {
        imports: [AuthModule, ConfigModule, ProductsModule, UsersModule],
        controllers: [AppController],
      },
    })
    .compile();

    productsService = moduleRef.get<ProductsService>(ProductsService);
    usersService = moduleRef.get<UsersService>(UsersService);

    app = moduleRef.createNestApplication();
    await app.init();
    let userMock = mockUser();
    user = {
      ...await createMockUser(app, userMock),
      password: userMock.password,
    };
  });

  afterEach(async() => {
    await app.close();
  });

  describe('Login', () => {
    it('throws 401 when username is not provided', async () => {
      await request(app.getHttpServer())
        .post('/login')
        .send({ password: mockUser().password })
        .expect(401);
    });

    it('throws 401 when password is not provided', async () => {
      await request(app.getHttpServer())
        .post('/login')
        .send({ username: mockUser().username })
        .expect(401);
    });

    it('throws 404 when username is wrong', async () => {
      await request(app.getHttpServer())
        .post('/login')
        .send({ username: mockUser().username, password: user.password })
        .expect(404);
    });

    it('throws 404 when password is wrong', async () => {
      await request(app.getHttpServer())
        .post('/login')
        .send({ username: user.username, password: mockUser().password })
        .expect(404);
    });

    it('logs user in', async () => {
      const { body: { data } } = await request(app.getHttpServer())
        .post('/login')
        .send({ username: user.username, password: user.password })
        .expect(200);
      expect(data.user).toEqual({ ...user, accessToken: undefined, password: undefined });
    });
  });

  describe('Deposit', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await request(app.getHttpServer())
        .post('/deposit')
        .send({ deposit: Coins[0] })
        .expect(401);
    });

    it('throws 400 when deposit is not provided', async () => {
      await request(app.getHttpServer())
        .post('/deposit')
        .set(headers(user))
        .expect(400);
    });

    it('throws 400 when deposit is not valid', async () => {
      await request(app.getHttpServer())
        .post('/deposit')
        .set(headers(user))
        .send({ deposit: 1 })
        .expect(400);
    });

    it('deposits a coin', async () => {
      const { body: { data } } = await request(app.getHttpServer())
        .post('/deposit')
        .set(headers(user))
        .send({ deposit: Coins[0] })
        .expect(200);
      expect(data).toEqual({
        deposit: Coins[0],
      });
    });
  });

  describe('Buy', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await usersService.deposit(user.id, 20);
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .post('/buy')
        .send({ amount: 1, productId: product.id })
        .expect(401);
    });

    it('throws 400 when product id is not provided', async () => {
      await request(app.getHttpServer())
        .post('/buy')
        .set(headers(user))
        .send({ amount: 1 })
        .expect(400);
    });

    it('throws 404 when product id is not valid', async () => {
      await request(app.getHttpServer())
        .post('/buy')
        .set(headers(user))
        .send({ amount: 1, productId: faker.datatype.number() })
        .expect(404);
    });

    it('throws 400 when amount is not provided', async () => {
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .post('/buy')
        .set(headers(user))
        .send({ productId: product.id })
        .expect(400);
    });

    it('throws 400 when amount is not valid', async () => {
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .post('/buy')
        .set(headers(user))
        .send({ amount: 0, productId: product.id })
        .expect(400);
    });

    it('throws 400 when user deposit is too low', async () => {
      const product = await productsService.create(user, mockProduct());
      await request(app.getHttpServer())
        .post('/buy')
        .set(headers(user))
        .send({ amount: 1, productId: product.id })
        .expect(400);
    });

    it('buys a product', async () => {
      await usersService.deposit(user.id, 20);
      const product = await productsService.create(user, {
        ...mockProduct(),
        amountAvailable: 10,
        cost: 2,
      });
      const { body: { data } } = await request(app.getHttpServer())
        .post('/buy')
        .set(headers(user))
        .send({ amount: 2, productId: product.id })
        .expect(200);
      expect(data).toEqual({
        total: 4,
        products: [{ id: product.id, amount: 2 }],
        change: [10, 5],
      });
    });
  });

  describe('Reset', () => {
    it('throws 401 when authorization token is not provided', async () => {
      await request(app.getHttpServer())
        .post('/reset')
        .expect(401);
    });

    it('resets user deposit', async () => {
      await request(app.getHttpServer())
        .post('/reset')
        .set(headers(user))
        .expect(200);
      const { deposit } = await usersService.getById(user.id);
      expect(deposit).toEqual(0);
    });
  });
});
