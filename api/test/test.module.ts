import { ClassSerializerInterceptor, ExecutionContext, ModuleMetadata } from '@nestjs/common';
import { APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { AppInterceptor } from '../src/app.interceptor';
import { AppValidationPipe } from '../src/app.validation-pipe';
import { AuthorizationGuard } from '../src/auth/guards/authorization.guard';
import { TestDatabaseModule } from './test-database.module';

export const TestModule = ({ user, entities, metadata }) =>
  Test.createTestingModule({
    ...metadata,
    imports: [
      TestDatabaseModule(entities),
      ...(metadata.imports || []),
    ],
    providers: [
      {
        provide: APP_PIPE,
        useClass: AppValidationPipe,
      },
      {
        provide: APP_INTERCEPTOR,
        useClass: ClassSerializerInterceptor,
      },
      {
        provide: APP_INTERCEPTOR,
        useClass: AppInterceptor,
      },
      ...(metadata.providers || []),
    ],
  });
  // .overrideGuard(AuthorizationGuard)
  // .useValue({
  //   canActivate: (context: ExecutionContext) => {
  //     const req = context.switchToHttp().getRequest();
  //     req.user = user;
  //     return true;
  //   },
  // });
