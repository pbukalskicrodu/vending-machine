import { useState } from 'react';
import { useLoginMutation } from '../services/api';
import { Box } from '../shared/Box';
import { Input } from '../shared/Input';
import { Button } from '../shared/Button';

export const Login = () => {
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });
  const [apiLogin] = useLoginMutation();

  const handleInputChange = ({ target: { name, value } }) => {
    setCredentials({
      ...credentials,
      [name]: value,
    });
  };

  const login = async () => {
    await apiLogin(credentials);
  }

  return (
    <div className="relative py-3 sm:max-w-xl sm:mx-auto">
      <Box title="Login">
        <Input label="Username" name="username" value={credentials.username} onChange={handleInputChange} />
        <Input classes="mt-4" name="password" type="password" label="Password" value={credentials.password} onChange={handleInputChange} />
        <div className="flex justify-center items-baseline">
          <Button classes="mt-4" label="Login" onClick={login} />
        </div>
      </Box>
    </div>
  );
};
