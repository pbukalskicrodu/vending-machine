import { useState } from 'react';
import { Box } from '../shared/Box';
import { Button } from '../shared/Button';
import { Input } from '../shared/Input';
import { Select } from '../shared/Select';
import { useRegisterMutation } from '../services/api';
import { Roles } from '../constants';

export const Register = () => {
  const initialForm = {
    username: '',
    password: '',
    role: Roles.Buyer,
  };
  const [form, setForm] = useState(initialForm);
  const [apiRegister] = useRegisterMutation();

  const handleInputChange = ({ target: { name, value } }) => {
    setForm({
      ...form,
      [name]: value,
    });
  };

  const register = async () => {
    await apiRegister(form);
    setForm(initialForm);
  };

  return (
    <div className="relative py-3 sm:max-w-xl sm:mx-auto">
      <Box title="Register">
        <div className="px-8 py-6">
          <Input label="Username" name="username" value={form.username} onChange={handleInputChange} />
          <Input classes="mt-4" name="password" type="password" label="Password" value={form.password} onChange={handleInputChange} />
          <Select
            classes="mt-6"
            name="role"
            label="Role"
            options={Object.entries(Roles).map(([k, v]) => ({ label: k, value: v }))}
            value={form.role}
            onChange={handleInputChange}
          />
          <div className="flex justify-center items-baseline">
            <Button classes="mt-4" label="Register" onClick={register} />
          </div>
        </div>
      </Box>
    </div>
  );
}
