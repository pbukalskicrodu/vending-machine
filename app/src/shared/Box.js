export const Box = ({ title, children }) => (
  <div className="min-w-96 mt-4 bg-white shadow-md rounded-lg">
    <div className="px-8 py-6">
      <span className="flex text-lg font-bold mb-4">{title}</span>
      {children}
    </div>
  </div>
);
