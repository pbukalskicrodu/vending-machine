export const Centered = ({ children }) => (
  <div className="min-h-screen bg-gray-100 text-gray-800 antialiased py-6 flex justify-center sm:py-12">
    {children}
  </div>
);
