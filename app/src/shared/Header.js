import { useDispatch, useSelector } from 'react-redux';
import { NavLink, useRouteMatch } from 'react-router-dom';
import { Roles } from '../constants';
import { logout } from '../redux/auth';
import { Button } from './Button';

export const Header = () => {
  const { path } = useRouteMatch();
  const user = useSelector(state => state.auth.user);
  const dispatch = useDispatch();

  return (
    <nav className="flex items-center justify-between flex-wrap bg-indigo-600 h-16 px-4">
      <div className="flex items-center flex-shrink-0 text-white mr-6">
        <span className="font-semibold text-xl tracking-tight">Vending machine</span>
      </div>
      {path.includes('dashboard') && (
        <div className="w-auto flex flex-grow items-center">
          <ul className="text-sm flex-grow">
            <li className="inline-block hover:text-white mr-4">
              <NavLink to="/dashboard/products" activeClassName="font-bold text-white">Products</NavLink>
            </li>
            <li className="inline-block hover:text-white mr-4">
              <NavLink to="/dashboard/me" activeClassName="font-bold text-white">My account</NavLink>
            </li>
          </ul>
          <div className="text-white">
            <span className="mr-2">Logged in as {user.username}</span>
            {user.role === Roles.Buyer && <span className="mr-2">({user.deposit} cents)</span>}
            <Button classes="ml-1 inline-block" label="Logout" onClick={() => dispatch(logout())} />
          </div>
        </div>
      )}
    </nav>
  );
};
