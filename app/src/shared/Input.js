export const Input = ({ classes, label, name, type = 'text', value, onChange }) => (
  <div className={classes}>
    <input
      type={type}
      placeholder={label}
      name={name}
      value={value}
      onChange={onChange}
      className="border w-full h-5 px-3 py-5 mt-2 hover:outline-none focus:outline-none focus:ring-1 focus:ring-indigo-400 rounded-md"
    />
  </div>
);
