import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { reset } from '../redux/alert';

export const Alert = () => {
  const { type, message } = useSelector(state => state.alert);
  const dispatch = useDispatch();

  useEffect(() => {
    if (message) {
      setTimeout(() => dispatch(reset()), 3000);
    }
  }, [message, dispatch]);

  return message && (
    <div className="flex justify-center w-full fixed mt-14 p-6">
      <div className={`center px-5 py-3 rounded-lg ${type === 'error' && 'bg-red-500'} ${type === 'success' && 'bg-green-500'}`}>
          <span className="text-white">
            {[].concat(message).map((line, index) => <div key={index}>{line}</div>)}
          </span>
      </div>
    </div>
  );
};
