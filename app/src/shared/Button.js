export const Button = ({ classes, label, onClick, disabled, danger }) => (
  <div className={classes}>
    <button
      disabled={disabled}
      onClick={onClick}
      className={`${danger ? 'bg-red-500 hover:bg-red-600' : ' bg-indigo-500 hover:bg-indigo-600'} text-white py-2 px-6 rounded-lg disabled:bg-gray-400`}
    >
      {label}
    </button>
  </div>
);
