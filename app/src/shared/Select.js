import ReactSelect from 'react-select';

export const Select = ({ classes, label, name, options = [], value, onChange }) => {
  const handleChange = ({ value }) => onChange({ target: { name, value } });

  return (
    <div className={classes}>
      <ReactSelect label={label} defaultValue={options[0]} value={value.label} options={options} onChange={handleChange} />
    </div>
  );
};
