import { useUser } from '../redux/selectors';
import { Roles } from '../constants';
import { Box } from '../shared/Box';

export const UserInfo = () => {
  const { username, deposit, role } = useUser();
  return (
    <Box title={username}>
      <>
        {role === Roles.Buyer && <div>Deposit: <b>{deposit}</b> cents</div>}
        <div>Role: <b>{role}</b></div>
      </>
    </Box>
  );
};
