import { useDispatch } from 'react-redux';
import { useDeleteMeMutation } from '../services/api';
import { logout } from '../redux/auth';
import { Box } from '../shared/Box';
import { Button } from '../shared/Button';

export const UserDelete = () => {
  const [apiDeleteUser] = useDeleteMeMutation();
  const dispatch = useDispatch();

  const deleteUser = async () => {
    await apiDeleteUser();
    dispatch(logout());
  }

  return (
    <Box title="Delete user">
      <Button classes="mt-4" label="Delete" onClick={deleteUser} />
    </Box>
  )
};
