import { useState } from 'react';
import { useUpdateMeMutation } from '../services/api';
import { Box } from '../shared/Box';
import { Input } from '../shared/Input';
import { Button } from '../shared/Button';

export const UserUpdate = () => {
  const initialForm = {
    username: '',
    password: '',
  };
  const [form, setForm] = useState(initialForm);
  const [apiUpdateUser] = useUpdateMeMutation();

  const handleInputChange = ({ target: { name, value } }) => {
    setForm({
      ...form,
      [name]: value,
    });
  };

  const update = async () => {
    await apiUpdateUser({
      ...(form.username.length && { username: form.username }),
      ...(form.password.length && { password: form.password }),
    });
    setForm(initialForm);
  }

  return (
    <Box title="Update user">
      <Input label="Username" name="username" value={form.username} onChange={handleInputChange} />
      <Input label="Password" type="password" name="password" value={form.password} onChange={handleInputChange} />
      <Button classes="mt-4" label="Update" onClick={update} />
    </Box>
  )
};
