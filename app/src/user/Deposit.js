import { useState } from 'react';
import { useDepositMutation, useResetMutation } from '../services/api';
import { Coins} from '../constants';
import { Box } from '../shared/Box';
import { Select } from '../shared/Select';
import { Button } from '../shared/Button';

export const Deposit = () => {
  const [newDeposit, setNewDeposit] = useState(Coins[0]);
  const [apiDeposit] = useDepositMutation();
  const [apiReset] = useResetMutation();

  const handleInputChange = ({ target: { name, value } }) => {
    setNewDeposit(value);
  };

  const update = async () => {
    await apiDeposit(+newDeposit);
  }

  const reset = async () => {
    await apiReset();
  }

  return (
    <Box title="Deposit coins">
      <div className="flex items-center">
        <span className="mr-2">Deposit more: </span>
        <Select
          label="Deposit"
          options={Coins.map(coin => ({ label: coin, value: coin }))}
          value={newDeposit}
          onChange={handleInputChange}
        />
        <Button label="Deposit" classes="ml-2 mt-2 mb-3" onClick={update} />
      </div>
      <Button label="Reset deposit" onClick={reset} />
    </Box>
  );
};
