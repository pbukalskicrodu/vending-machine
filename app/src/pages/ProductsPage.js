import { useSelector } from 'react-redux';
import { Header } from '../shared/Header';
import { Centered } from '../shared/Centered';
import { Roles } from '../constants';
import { BuyerProduct } from '../products/BuyerProduct';
import { SellerProduct } from '../products/SellerProduct';
import { useGetProductsQuery } from '../services/api';

export const ProductsPage = () => {
  useGetProductsQuery();
  const products = useSelector(state => state.products.products);
  const user = useSelector(state => state.auth.user);
  return (
    <>
      <Header />
      <Centered>
        <div className="flex-shrink-0">
          {user.role === Roles.Buyer && products && products.map(product => (
            <BuyerProduct key={product.id} product={product} />
          ))}
          {user.role === Roles.Seller && <SellerProduct />}
          {user.role === Roles.Seller && products && products.map(product => (
            <SellerProduct key={product.id} product={product} />
          ))}
        </div>
      </Centered>
    </>
  );
};
