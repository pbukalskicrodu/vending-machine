import { useSelector } from 'react-redux';
import { Redirect, Route, useRouteMatch } from 'react-router-dom';
import { useGetMeQuery } from '../services/api';
import { ProductsPage } from './ProductsPage';
import { UserPage } from './UserPage';

export const DashboardPage = () => {
  const { path } = useRouteMatch();
  useGetMeQuery();
  const { user, logout } = useSelector(state => state.auth);
  if (logout) {
    return <Redirect to="/" />;
  } else if (user) {
    return (
      <div>
        <Route path={`${path}/products`} component={ProductsPage} />
        <Route path={`${path}/me`} component={UserPage} />
      </div>
    );
  }
  return null;
};
