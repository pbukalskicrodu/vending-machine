import { Roles } from '../constants';
import { useUser } from '../redux/selectors';
import { Header } from '../shared/Header';
import { Centered } from '../shared/Centered';
import { UserInfo } from '../user/UserInfo';
import { Deposit } from '../user/Deposit';
import { UserUpdate } from '../user/UserUpdate';
import { UserDelete } from '../user/UserDelete';

export const UserPage = () => {
  const { role } = useUser();
  return (
    <>
      <Header />
      <Centered>
        <div className="flex-shrink-0">
          <UserInfo />
          {role === Roles.Buyer && <Deposit />}
          <UserUpdate />
          <UserDelete />
        </div>
      </Centered>
    </>
  );
};
