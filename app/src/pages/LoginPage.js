import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import { Header } from '../shared/Header';
import { Centered } from '../shared/Centered';
import { Login } from '../login/Login';
import { Register } from '../login/Register';

export const LoginPage = () => {
  const user = useSelector(state => state.auth.user);
  return user ? (
    <Redirect to="/dashboard/products" />
  ) : (
    <>
      <Header />
      <Centered>
        <div className="flex-shrink-0">
          <Login/>
        </div>
        <div className="px-10"></div>
        <div className="flex-shrink-0">
          <Register/>
        </div>
      </Centered>
    </>
  );
};
