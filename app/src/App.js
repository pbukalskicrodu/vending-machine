import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Alert } from './shared/Alert';
import { DashboardPage } from './pages/DashboardPage';
import { LoginPage } from './pages/LoginPage';

const App = () => (
  <>
    <Alert />
    <BrowserRouter>
      <Switch>
        <Route path="/dashboard" component={DashboardPage} />
        <Route path="/" component={LoginPage} />
      </Switch>
    </BrowserRouter>
  </>
);

export default App;
