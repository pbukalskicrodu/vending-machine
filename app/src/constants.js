export const Coins = [5, 10, 20, 50, 100];

export const Roles = {
  Buyer: 'buyer',
  Seller: 'seller',
};
