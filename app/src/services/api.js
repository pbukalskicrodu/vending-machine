import { isRejectedWithValue } from '@reduxjs/toolkit';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { set } from '../redux/alert';

const transformResponse = ({ data }) => data;

export const api = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:3000/v1',
    prepareHeaders: headers => {
      const accessToken = localStorage.getItem('accessToken');
      if (accessToken) {
        headers.set('Authorization', `Bearer ${accessToken}`);
      }
      return headers;
    },
  }),
  refetchOnMountOrArgChange: true,
  endpoints: builder => ({
    login: builder.mutation({
      query: credentials => ({
        url: '/login',
        method: 'POST',
        body: credentials,
      }),
      transformResponse,
    }),
    getMe: builder.query({
      query: () => '/users/me',
      transformResponse,
    }),
    getUsers: builder.query({
      query: () => '/users',
      transformResponse,
    }),
    register: builder.mutation({
      query: user => ({
        url: '/users',
        method: 'POST',
        body: user,
      }),
      transformResponse,
    }),
    deposit: builder.mutation({
      query: deposit => ({
        url: '/deposit',
        method: 'POST',
        body: { deposit },
      }),
      transformResponse,
    }),
    buy: builder.mutation({
      query: data => ({
        url: '/buy',
        method: 'POST',
        body: data,
      }),
      transformResponse,
    }),
    reset: builder.mutation({
      query: () => ({
        url: '/reset',
        method: 'POST',
      }),
      transformResponse,
    }),
    updateMe: builder.mutation({
      query: user => ({
        url: '/users/me',
        method: 'PUT',
        body: user,
      }),
      transformResponse,
    }),
    deleteMe: builder.mutation({
      query: () => ({
        url: '/users/me',
        method: 'DELETE',
      }),
      transformResponse,
    }),
    getProducts: builder.query({
      query: () => '/products',
      transformResponse,
    }),
    createProduct: builder.mutation({
      query: product => ({
        url: '/products',
        method: 'POST',
        body: product,
      }),
      transformResponse,
    }),
    updateProduct: builder.mutation({
      query: product => ({
        url: `/products/${product.id}`,
        method: 'PUT',
        body: product,
      }),
      transformResponse,
    }),
    deleteProduct: builder.mutation({
      query: product => ({
        url: `/products/${product.id}`,
        method: 'DELETE',
      }),
      transformResponse,
    }),
  }),
});

export const {
  useLoginMutation,
  useGetMeQuery,
  useGetUsersQuery,
  useRegisterMutation,
  useDepositMutation,
  useBuyMutation,
  useResetMutation,
  useUpdateMeMutation,
  useDeleteMeMutation,
  useGetProductsQuery,
  useCreateProductMutation,
  useUpdateProductMutation,
  useDeleteProductMutation,
} = api;

export const errorHandler = api => next => action => {
  if (isRejectedWithValue(action)) {
    api.dispatch(
      set({
        type: 'error',
        message: action.payload.data.message,
      }));
  }
  return next(action);
};
