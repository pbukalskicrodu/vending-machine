import { useState } from 'react';
import {
  useCreateProductMutation,
  useDeleteProductMutation,
  useUpdateProductMutation,
} from '../services/api';
import { Box } from '../shared/Box';
import { Input } from '../shared/Input';
import { Button } from '../shared/Button';

export const SellerProduct = ({ product = {} }) => {
  const getInitialForm = () => ({
    amountAvailable: '',
    cost: '',
    productName: '',
  });
  const [form, setForm] = useState(product);
  const [apiCreateProduct] = useCreateProductMutation();
  const [apiUpdateProduct] = useUpdateProductMutation();
  const [apiDeleteProduct] = useDeleteProductMutation();

  const handleInputChange = ({ target: { name, value, valueAsNumber } }) => {
    setForm({
      ...form,
      [name]: valueAsNumber || value,
    });
  };

  const productExists = Object.keys(product).length;

  const createProduct = async () => {
    await apiCreateProduct(form);
    setForm(getInitialForm());
  }

  const updateProduct = async () => {
    await apiUpdateProduct({ ...product, ...form });
  }

  const deleteProduct = async () => {
    await apiDeleteProduct(product);
  };

  return (
    <Box title={form.productName || 'Create product'}>
      <div className="flex justify-end items-baseline">
        <label className="mr-2">Amount available</label>
        <Input type="number" name="amountAvailable" label="Amount available" value={form.amountAvailable} onChange={handleInputChange} />
      </div>
      <div className="flex justify-end items-baseline">
        <label className="mr-2">Cost</label>
        <Input type="number" name="cost" label="Cost" value={form.cost} onChange={handleInputChange} />
      </div>
      <div className="flex justify-end items-baseline">
        <label className="mr-2">Name</label>
        <Input type="string" name="productName" label="Name" value={form.productName} onChange={handleInputChange} />
      </div>
      <div className="flex justify-end mt-4">
        {productExists ? (
          <>
            <Button label="Delete" onClick={deleteProduct} danger={true} classes="mr-4" />
            <Button label="Update" onClick={updateProduct} />
          </>
        ) : (
          <Button label="Create" onClick={createProduct} />
        )}
      </div>
    </Box>
  );
};
