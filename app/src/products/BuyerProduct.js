import { useState } from 'react';
import { useBuyMutation } from '../services/api';
import { Box } from '../shared/Box';
import { Input } from '../shared/Input';
import { Button } from '../shared/Button';
import { useSelector } from 'react-redux';

export const BuyerProduct = ({ product: { id, amountAvailable, cost, productName, sellerId } }) => {
  const [amount, setAmount] = useState(1);
  const { deposit } = useSelector(state => state.auth.user);
  const [buy] = useBuyMutation();

  const handleInputChange = ({ target: { valueAsNumber } }) => {
    setAmount(valueAsNumber);
  };

  const cantBuy = deposit <= 0 || amount <= 0 || amount > amountAvailable || amount * cost > deposit;

  return (
    <Box title={productName}>
      <div>Cost: {cost}</div>
      <div>Amount available: {amountAvailable}</div>
      <div className="flex justify-end items-baseline">
        <label className="mr-2">Amount</label>
        <Input type="number" label="Amount" value={amount} onChange={handleInputChange} />
        <Button classes="ml-4" label="Buy" onClick={() => buy({ amount, productId: id })} disabled={cantBuy} />
      </div>
    </Box>
  );
};
