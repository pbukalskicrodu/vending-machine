import { createSlice } from '@reduxjs/toolkit';
import { api } from '../services/api';

export const productsSlice = createSlice({
  name: 'products',
  initialState: {
    products: [],
  },
  reducers: {
    setProducts: (state, action) => {
      state.products = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addMatcher(
      api.endpoints.buy.matchFulfilled,
      (state, { payload: { products } }) => {
        for (const product of products) {
          const index = state.products.findIndex(p => p.id === product.id);
          if (index >= 0) {
            state.products[index].amountAvailable -= product.amount;
          }
        }
      },
    );
    builder.addMatcher(
      api.endpoints.getProducts.matchFulfilled,
      (state, { payload: products }) => {
        state.products = products;
      },
    );
    builder.addMatcher(
      api.endpoints.createProduct.matchFulfilled,
      (state, { payload: product }) => {
        state.products.push(product);
      },
    );
    builder.addMatcher(
      api.endpoints.updateProduct.matchFulfilled,
      (state, { payload: product }) => {
        const index = state.products.findIndex(p => p.id === product.id);
        state.products[index] = product;
      },
    );
    builder.addMatcher(
      api.endpoints.deleteProduct.matchFulfilled,
      (state, { payload: id }) => {
        const index = state.products.findIndex(p => p.id === id);
        if (index >= 0) {
          state.products.splice(index, 1);
        }
      },
    );
  },
});

export const { setProducts } = productsSlice.actions;

export const productsReducer = productsSlice.reducer;
