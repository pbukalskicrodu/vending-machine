import { configureStore } from '@reduxjs/toolkit';
import { api, errorHandler } from '../services/api';
import { alertReducer } from './alert';
import { authReducer } from './auth';
import { productsReducer } from './products';

export const store = configureStore({
  reducer: {
    [api.reducerPath]: api.reducer,
    alert: alertReducer,
    auth: authReducer,
    products: productsReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(api.middleware).concat(errorHandler),
});
