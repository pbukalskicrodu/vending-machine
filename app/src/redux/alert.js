import { createSlice } from '@reduxjs/toolkit';
import { api } from '../services/api';
import { alerts } from './alerts';

export const alertSlice = createSlice({
  name: 'alert',
  initialState: {
    type: null,
    message: null,
  },
  reducers: {
    set: (state, { payload: { type, message } }) => {
      state.type = type;
      state.message = message;
    },
    reset: state => {
      state.type = null;
      state.message = null;
    },
  },
  extraReducers: builder => {
    for (const [endpoint, messageBuilder] of Object.entries(alerts)) {
      builder.addMatcher(
        api.endpoints[endpoint].matchFulfilled,
        (state, action) => {
          state.type = 'success';
          state.message = messageBuilder(action.payload);
        },
      );
    }
  },
});

export const { set, reset } = alertSlice.actions;

export const alertReducer = alertSlice.reducer;
