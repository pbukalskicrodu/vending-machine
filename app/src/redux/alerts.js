export const alerts = {
  register: () => 'Registration successful, you can now login',
  buy: ({ change, total }) => `Product has been successfully bought for a total of ${total}${change.length ? ` and received coins: ${change.join(',')}`: ''}`,
  createProduct: () => 'Product has been successfully created',
  updateProduct: () => 'Product has been successfully updated',
  deleteProduct: () => 'Product has been successfully deleted',
  deposit: ({ deposit }) => `${deposit} coins has been successfully deposited`,
  reset: () => 'Your deposit has been successfully reset',
  updateMe: () => 'User has been successfully updated',
  deleteMe: () => 'User has been successfully deleted',
};
