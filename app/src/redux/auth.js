import { createSlice } from '@reduxjs/toolkit';
import { api } from '../services/api';

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    user: null,
    logout: false,
  },
  reducers: {
    logout: state => {
      state.user = null;
      state.logout = true;
    },
  },
  extraReducers: builder => {
    builder.addMatcher(
      api.endpoints.login.matchFulfilled,
      (state, { payload: { accessToken, user } }) => {
        localStorage.setItem('accessToken', accessToken);
        state.user = user;
        state.logout = false;
      },
    );
    builder.addMatcher(
      api.endpoints.deposit.matchFulfilled,
      (state, { payload: { deposit } }) => {
        state.user.deposit = deposit;
      },
    );
    builder.addMatcher(
      api.endpoints.buy.matchFulfilled,
      state => {
        state.user.deposit = 0;
      },
    );
    builder.addMatcher(
      api.endpoints.reset.matchFulfilled,
      state => {
        state.user.deposit = 0;
      },
    );
    builder.addMatcher(
      api.endpoints.getMe.matchFulfilled,
      (state, { payload: user }) => {
        state.user = user;
        state.logout = false;
      },
    );
    builder.addMatcher(
      api.endpoints.updateMe.matchFulfilled,
      (state, { payload: user }) => {
        state.user = user;
      },
    );
  },
});

export const { logout } = authSlice.actions;

export const authReducer = authSlice.reducer;
